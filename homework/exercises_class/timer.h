/*
 * timer.h
 *  Created on: 05/08/2015
 *      Author: pperezm
 */
/*--------------------------------------------------------------------
* Actividad de programación: Listas encadenadas
* Fecha: 25-Febrero-2020
* Autor:Alexis Garcia Gutierrez
*

*--------------------------------------------------------------------*/

#ifndef TIMER_H_
#define TIMER_H_

#include <string>
#include <sstream>

class Timer {
private:
	int hours, minutes;

public:
	Timer();
	Timer(int, int);
	Timer(const Timer&);

	int getHours() const;
	int getMinutes() const;
	std::string toString() const;

	void operator= (const Timer&);
	void operator+= (const Timer&);
};

Timer::Timer() : hours(0), minutes(0) {}

Timer::Timer(int hh, int mm) {
    hours = (hh + (mm/60))%24;
    minutes = mm%60;
    
}

Timer::Timer(const Timer &t) {
hours = t.getHours();
minutes= t.getMinutes();
}

int Timer::getHours() const {
return hours;
}

int Timer::getMinutes() const {
return minutes;
}

std::string Timer::toString() const {
	 std::stringstream aux;

	 if (hours < 10) {
		 aux << "0";
	 }
	 aux << hours << ":";
	 if (minutes < 10) {
		 aux << "0";
	 }
	 aux << minutes;
	 return aux.str();
}

void Timer::operator= (const Timer &right) {
hours = right.getHours();
minutes= right.getMinutes();
}

void Timer::operator+= (const Timer &right) {
    hours = (hours + right.getHours()+ ((minutes + right.getMinutes())/60))%24;
    minutes = ((minutes + right.getMinutes()))%60;
}

bool operator== (const Timer &left, const Timer &right) {
    if ((left.getHours() == right.getHours()) && (left.getMinutes() == right.getMinutes())){
    return true;
    }
    else{
        return false;
    }
}

bool operator> (const Timer &left, const Timer &right) {
    if ((left.getHours() > right.getHours()) || ((left.getHours() == right.getHours()) && (left.getMinutes() > right.getMinutes() )))
    {
    return true;
    }
    else{
        return false;
    }
}

#endif /* TIMER_H_ */
