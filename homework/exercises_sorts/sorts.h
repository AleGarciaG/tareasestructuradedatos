/*
 * sorts.h
 *
 *  Created on: 05/08/2015
 *      Author: pperezm
 */
/*
 *Actividad de programación: Algoritmos de ordenamiento
 *
 *  Created on: 04/02/2020
 *      Author: Alexis Garcia Gutierrez Matricula A01703422
 */

#ifndef SORTS_H_
#define SORTS_H_

#include "../includes/exception.h"
#include <vector>
#include <list>

template <class T>
class sorts {
private:
	void swap(std::vector<T>&, int, int);
	
public:
	std::vector<T> bucket_sort(const std::vector<T>&);
	std::list<T>   merge_list(const std::list<T>&, const std::list<T>&);
};

template <class T>
void sorts<T>::swap(std::vector<T> &v, int i, int j) {
	T aux = v[i];
	v[i] = v[j];
	v[j] = aux;
}

template <class T>
std::vector<T> sorts<T>::bucket_sort(const std::vector<T> &source) {
	typename  std::list<T>::iterator itr;
	std::vector<T> v(source);
	std::vector<T> v0;
	std::vector<T> v1;
	std::vector<T> v2;
	std::vector<T> v3;
	std::vector<T> v4;
	std::vector<T> v5;
	std::vector<T> v6;
	std::vector<T> v7;
	std::vector<T> v8;
	std::vector<T> v9;
	std::vector<T> s;

	for(int i  =  0; i < v.size(); i++){
		if (v[i]<10){
			v0.push_back(v[i]);
		}else if (v[i]<20){
			v1.push_back(v[i]);
		}else if (v[i]<30){
			v2.push_back(v[i]);
		}else if (v[i]<40){
			v3.push_back(v[i]);
		}else if (v[i]<50){
			v4.push_back(v[i]);
		}else if (v[i]<60){
			v5.push_back(v[i]);
		}else if (v[i]<70){
			v6.push_back(v[i]);
		}else if (v[i]<80){
			v7.push_back(v[i]);
		}else if (v[i]<90){
			v8.push_back(v[i]);
		}else if (v[i]<100){
			v9.push_back(v[i]);
		}		
	}
	for(int i  = v0.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v0[j] > v0[j+1]){
                swap(v0, j, j+1);
            }
        }
	}
	for(int i  = v1.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v1[j] > v1[j+1]){
                swap(v1, j, j+1);
            }
        }
	}
	for(int i  = v2.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v2[j] > v2[j+1]){
                swap(v2, j, j+1);
            }
        }
	}
	for(int i  = v3.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v3[j] > v3[j+1]){
                swap(v3, j, j+1);
            }
        }
	}
	for(int i  = v4.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v4[j] > v4[j+1]){
                swap(v4, j, j+1);
            }
        }
	}
	for(int i  = v5.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v5[j] > v5[j+1]){
                swap(v5, j, j+1);
            }
        }
	}
	for(int i  = v6.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v6[j] > v6[j+1]){
                swap(v6, j, j+1);
            }
        }
	}
	for(int i  = v7.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v7[j] > v7[j+1]){
                swap(v7, j, j+1);
            }
        }
	}
	for(int i  = v8.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v8[j] > v8[j+1]){
                swap(v8, j, j+1);
            }
        }
	}
	for(int i  = v9.size() -1; i > 0; i--){
        for(int j = 0;  j<i; j++){
            if(v9[j] > v9[j+1]){
                swap(v9, j, j+1);
            }
        }
	}
    for(int j = 0;  j<v0.size(); j++){
		s.push_back(v0[j]);
    }
	
    for(int j = 0;  j<v1.size(); j++){
		s.push_back(v1[j]);
    }
	
    for(int j = 0;  j<v2.size(); j++){
		s.push_back(v2[j]);
    }
    for(int j = 0;  j<v3.size(); j++){
		s.push_back(v3[j]);
    }
    for(int j = 0;  j<v4.size(); j++){
		s.push_back(v4[j]);
    }
    for(int j = 0;  j<v5.size(); j++){
		s.push_back(v5[j]);
    }	
    for(int j = 0;  j<v6.size(); j++){
		s.push_back(v6[j]);
    }
    for(int j = 0;  j<v7.size(); j++){
		s.push_back(v7[j]);
    }
    for(int j = 0;  j<v8.size(); j++){
		s.push_back(v8[j]);
    }
    for(int j = 0;  j<v9.size(); j++){
		s.push_back(v9[j]);
    }	
	
	return s;
}

template <class T>
std::list<T> sorts<T>::merge_list(const std::list<T> &lst1, const std::list<T> &lst2) {
	typename std::list<T>::const_iterator itr1, itr2;
	itr1 = lst1.begin();
	itr2 = lst2.begin();
	
	std::list<T> result;

	while ((itr1!=lst1.end()) && (itr2!=lst2.end()))
	{	
		int data1 = *itr1;
		int data2 = *itr2;

		if (data1<=data2 ){
			result.push_back(data1);
			itr1++;
		}else
		{
			result.push_back(data2);
			itr2++;
		}
	}
	while(itr1!=lst1.end()){
			int data1 = *itr1;
			result.push_back(data1);
			itr1++;
	}
	while(itr2!=lst2.end()){
			int data2 = *itr2;
			result.push_back(data2);
			itr2++;
	}
	return result;
}

#endif /* SORTS_H_ */
